function Boton1()
{
	var num1,
		num2;

	resultado = document.getElementById("resultado");
	num1 = 4;
	num2 = 10;
	resultado.innerHTML +=
		"<br> La suma es "+ 
			(num1+num2) + "<br>";
}

function Boton2()
{
	var num1,
		num2;

	resultado = document.getElementById("resultado");
	num1 = 4;
	num2 = "hola";
	resultado.innerHTML +=
		"<br> La suma es "+ 
			(num1+num2) + "<br>";
}

function Boton3()
{
	var num1,
	    num2;

	resultado = document.getElementById("resultado");
	num1 = "hola";
	num2 = 10;
	resultado.innerHTML +=
		"<br> La suma es "+ 
			(num1+num2) + "<br>";
}

function Boton4()
{
	var num1,
		num2;

	resultado = document.getElementById("resultado");
	num1 = "hola";
	num2 = "hola";
	resultado.innerHTML +=
		"<br> La suma es "+ 
			(num1+num2) + "<br>";
}