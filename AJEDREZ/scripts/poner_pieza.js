function mostrarPiezas()
{
	var pieza,
	    piezas;

    piezas = document.getElementById('figura').value;

    var mostrar,
    posicionfila,
    posicioncol,
    areacol,
    areafila;

    if (isNaN(document.getElementById('posicioncol').value))
    {
        alert("Sólo se pueden poner números");
        return false;
    }

    if (isNaN(document.getElementById('posicionfila').value))
    {
        alert("Sólo se pueden poner números");
        return false;
    }



    posicioncol = (document.getElementById('posicioncol').value - 1);
    posicionfila = (document.getElementById('posicionfila').value - 1);

    areacol = (document.getElementById('posicioncol').value - 1);
    areafila = (document.getElementById('posicionfila').value - 1);    

    var cntFila,
    cntCol,
    veces = 8,
    tablero_con_pieza;

    var arrayTablero = new Array(veces);  //array que contendrá el tablero
    for(var cnt=0; cnt<veces; cnt++)
      arrayTablero[cnt] = new Array(veces);
    
    tablero_con_pieza = document.getElementById('tablero_con_pieza');

    if (posicionfila <= -1 || posicioncol <= -1)
    {
        alert("Los campos 'posicion fila' y 'posicion columna' no pueden ser menor de 1");
        return false;
    }

    if (posicionfila >= 8 || posicioncol >= 8)
    {
        alert("Los campos 'posicion fila' y 'posicion columna' no pueden ser mayor de 8");
        return false;
    }

    for(cntCol=0; cntCol < veces; cntCol++)
    {                                                     
    for(cntFila=0; cntFila<veces; cntFila++)
        {
            if( ((cntFila % 2 != 0) && (cntCol % 2 == 0)) || ((cntFila % 2 == 0) && (cntCol % 2 != 0)) )
            {
                arrayTablero[cntCol][cntFila] = "<img src='img/negro.png'>";
            }
            else{
                arrayTablero[cntCol][cntFila] = "<img src='img/blanco.png'>";
            }
        }           
    }

    
    if (piezas == "Torre")
    {      
            for ( areafila; areafila < 8; areafila++)
            arrayTablero[posicioncol][areafila] = "<img src='img/rojo.png'>";

            for ( areafila; areafila >= 0; areafila--)
            arrayTablero[posicioncol][areafila] = "<img src='img/rojo.png'>";

            for ( areacol; areacol < 8; areacol++)
            arrayTablero[areacol][posicionfila] = "<img src='img/rojo.png'>";

            for ( areacol =areacol-1; areacol >= 0; areacol--)
            arrayTablero[areacol][posicionfila] = "<img src='img/rojo.png'>";

        arrayTablero[posicioncol][posicionfila] = "<img src='img/torre.png'>";
    }

    if (piezas == "Caballo")
    {
        arrayTablero[areacol + 1][areafila + 2] = "<img src='img/rojo.png'>";
        arrayTablero[areacol - 1][areafila + 2] = "<img src='img/rojo.png'>";
        arrayTablero[areacol + 1][areafila - 2] = "<img src='img/rojo.png'>";
        arrayTablero[areacol - 1][areafila - 2] = "<img src='img/rojo.png'>";
        arrayTablero[areacol + 2][areafila + 1] = "<img src='img/rojo.png'>";
        arrayTablero[areacol - 2][areafila + 1] = "<img src='img/rojo.png'>";
        arrayTablero[areacol + 2][areafila - 1] = "<img src='img/rojo.png'>";
        arrayTablero[areacol - 2][areafila - 1] = "<img src='img/rojo.png'>";

        arrayTablero[posicioncol][posicionfila] = "<img src='img/caballo.png'>";
    }

    if (piezas == "Alfil")
    {   
            var areafilaInicial = areafila;
            var areacolInicial = areacol;
            while (( areafila < 8) && (areacol < 8))
            {   
                    arrayTablero[areacol++][areafila++] = "<img src='img/rojo.png'>";
            }

            areafila = areafilaInicial;
            areacol = areacolInicial;
            while (( areafila >= 0) && (areacol >= 0))
            {   
                    arrayTablero[areacol--][areafila--] = "<img src='img/rojo.png'>";
            }

            areafila = areafilaInicial;
            areacol = areacolInicial;
            while (( areafila >= 0) && (areacol < 8))
            {
                arrayTablero[areacol++][areafila--] = "<img src='img/rojo.png'>";
            }

            areafila = areafilaInicial;
            areacol = areacolInicial;
            while (( areafila < 8) && (areacol >= 0))
            {   
                arrayTablero[areacol--][areafila++] = "<img src='img/rojo.png'>";
            }
          

        arrayTablero[posicioncol][posicionfila] = "<img src='img/alfil.png'>";
    }

    if (piezas == "Peon")
    {
        if (areacol == 0)
        {
            alert("El peón no puede avanzar hacia adelante");
            arrayTablero[posicioncol][posicionfila] = "<img src='img/peon.png'>";
        }
        else{
            arrayTablero[areacol-1][posicionfila] = "<img src='img/rojo.png'>";
            arrayTablero[areacol-1][posicionfila+1] = "<img src='img/rojo.png'>";
            arrayTablero[areacol-1][posicionfila-1] = "<img src='img/rojo.png'>";

            arrayTablero[posicioncol][posicionfila] = "<img src='img/peon.png'>";
        }
    }

    if (piezas == "Reina")
    {
        var areafilaInicial = areafila;
        var areacolInicial = areacol;
        for ( areafila; areafila < 8; areafila++)
            arrayTablero[posicioncol][areafila] = "<img src='img/rojo.png'>";

            for ( areafila; areafila >= 0; areafila--)
            arrayTablero[posicioncol][areafila] = "<img src='img/rojo.png'>";

            for ( areacol; areacol < 8; areacol++)
            arrayTablero[areacol][posicionfila] = "<img src='img/rojo.png'>";

            for ( areacol =areacol-1; areacol >= 0; areacol--)
            arrayTablero[areacol][posicionfila] = "<img src='img/rojo.png'>";

            areafila = areafilaInicial;
            areacol = areacolInicial;
            while (( areafila < 8) && (areacol < 8))
            {   
                    arrayTablero[areacol++][areafila++] = "<img src='img/rojo.png'>";
            }

            areafila = areafilaInicial;
            areacol = areacolInicial;
            while (( areafila >= 0) && (areacol >= 0))
            {   
                    arrayTablero[areacol--][areafila--] = "<img src='img/rojo.png'>";
            }

            areafila = areafilaInicial;
            areacol = areacolInicial;
            while (( areafila >= 0) && (areacol < 8))
            {
                arrayTablero[areacol++][areafila--] = "<img src='img/rojo.png'>";
            }

            areafila = areafilaInicial;
            areacol = areacolInicial;
            while (( areafila < 8) && (areacol >= 0))
            {   
                arrayTablero[areacol--][areafila++] = "<img src='img/rojo.png'>";
            }

        arrayTablero[posicioncol][posicionfila] = "<img src='img/reina.png'>";
    }
    
    if (piezas == "Rey")
    {
    	if (areafila == 0 && areacol == 0)
    	{
    		arrayTablero[areacol+1][areafila] = "<img src='img/rojo.png'>";
    		arrayTablero[areacol][areafila+1] = "<img src='img/rojo.png'>";
    		arrayTablero[areacol+1][areafila+1] = "<img src='img/rojo.png'>";

    		arrayTablero[posicioncol][posicionfila] = "<img src='img/rey.png'>";
    	}
        else if (areacol == 0) 
        {
            arrayTablero[areacol+1][areafila] = "<img src='img/rojo.png'>";
            arrayTablero[areacol+1][areafila+1] = "<img src='img/rojo.png'>";
            arrayTablero[areacol+1][areafila-1] = "<img src='img/rojo.png'>";
            arrayTablero[areacol][areafila+1] = "<img src='img/rojo.png'>";
            arrayTablero[areacol][areafila-1] = "<img src='img/rojo.png'>";

            arrayTablero[posicioncol][posicionfila] = "<img src='img/rey.png'>";
        }
        else if (areacol == 7) 
        {
            arrayTablero[areacol-1][areafila] = "<img src='img/rojo.png'>";
            arrayTablero[areacol-1][areafila+1] = "<img src='img/rojo.png'>";
            arrayTablero[areacol-1][areafila-1] = "<img src='img/rojo.png'>";
            arrayTablero[areacol][areafila+1] = "<img src='img/rojo.png'>";
            arrayTablero[areacol][areafila-1] = "<img src='img/rojo.png'>";

            arrayTablero[posicioncol][posicionfila] = "<img src='img/rey.png'>";
        }
    	else if (areafila == 0 && areacol == 7)
    	{
    		arrayTablero[areacol][areafila+1] = "<img src='img/rojo.png'>";
    		arrayTablero[areacol-1][areafila] = "<img src='img/rojo.png'>";
    		arrayTablero[areacol-1][areafila+1] = "<img src='img/rojo.png'>";

    		arrayTablero[posicioncol][posicionfila] = "<img src='img/rey.png'>";
    	}
    	else if (areafila == 7 && areacol == 0)
    	{
    		arrayTablero[areacol][areafila-1] = "<img src='img/rojo.png'>";
    		arrayTablero[areacol+1][areafila] = "<img src='img/rojo.png'>";
    		arrayTablero[areacol+1][areafila-1] = "<img src='img/rojo.png'>";

    		arrayTablero[posicioncol][posicionfila] = "<img src='img/rey.png'>";
    	}
    	else if (areafila == 7 && areacol == 7)
    	{
    		arrayTablero[areacol][areafila-1] = "<img src='img/rojo.png'>";
    		arrayTablero[areacol-1][areafila] = "<img src='img/rojo.png'>";
    		arrayTablero[areacol-1][areafila-1] = "<img src='img/rojo.png'>";

    		arrayTablero[posicioncol][posicionfila] = "<img src='img/rey.png'>";
    	}
    	else
    	{
        arrayTablero[areacol-1][areafila+1] = "<img src='img/rojo.png'>";
        arrayTablero[areacol-1][areafila-1] = "<img src='img/rojo.png'>";
        arrayTablero[areacol+1][areafila+1] = "<img src='img/rojo.png'>";
        arrayTablero[areacol+1][areafila-1] = "<img src='img/rojo.png'>";
        arrayTablero[posicioncol][areafila+1] = "<img src='img/rojo.png'>";
        arrayTablero[posicioncol][areafila-1] = "<img src='img/rojo.png'>";
        arrayTablero[areacol+1][posicionfila] = "<img src='img/rojo.png'>";
        arrayTablero[areacol-1][posicionfila] = "<img src='img/rojo.png'>";

        arrayTablero[posicioncol][posicionfila] = "<img src='img/rey.png'>";
    	}
    } 


    for(cntCol=0; cntCol < veces; cntCol++)
    {                                                     
        for(cntFila=0; cntFila<veces; cntFila++)
        {
            tablero_con_pieza.innerHTML += arrayTablero[cntCol][cntFila];
        }
    }   
}