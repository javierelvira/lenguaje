
//Esta funcion lo que hace es sacar el tablero vacío sin ninguna pieza dentro de él

function tablero()
{ 
  //variables que utilizaremos para crear el tablero
	var cntFila,
	cntCol,
	veces = 8,
	tablero;

	var arrayTablero = new Array(veces);  //array que contendrá el tablero
	for(var cnt=0; cnt<veces; cnt++)
      arrayTablero[cnt] = new Array(veces); // este for sirver para crear las 64 casillas
	
	tablero = document.getElementById('tablero'); //sirve para mandar el tablero al html


	for(cntCol=0; cntCol < veces; cntCol++)
	{                                                     
    for(cntFila=0; cntFila<veces; cntFila++) //Estos dos for sirven para rellenar el tablero alternando blanco y negro de forma que no haya dos juntos ni en fila ni en columna.
        {
    		if( ((cntFila % 2 != 0) && (cntCol % 2 == 0)) || ((cntFila % 2 == 0) && (cntCol % 2 != 0)) )
    		{
      			arrayTablero[cntCol][cntFila] = "<img src='img/negro.png'>";
    		}
    		else{
      			arrayTablero[cntCol][cntFila] = "<img src='img/blanco.png'>";
    		}
 		}			
  }


  for(cntCol=0; cntCol < veces; cntCol++)
  {                                                     
    for(cntFila=0; cntFila<veces; cntFila++)
    {
      tablero.innerHTML += arrayTablero[cntCol][cntFila]; //Esto hace que se imprima el tableros
    }
  }
}
